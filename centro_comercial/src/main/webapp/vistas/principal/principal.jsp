
<%-- 
    Document   : login
    Created on : 6/10/2021, 05:12:15 PM
    Author     : Lenovo L420
--%>
<%@include file="/vistas/header/header.jsp"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.develop.model.ProductoM"%>
<%@ page import="com.develop.DAO.ProductoDAO"%>

<!DOCTYPE html>
<html>

    <head>
        <title>My Awesome Login Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Untitled</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="principal.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link rel="stylesheet" href="<%=basePath %>login.css">
         <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
         <style type="text/css">
         body{margin-top:20px;
    background:#eee;
}
/* E-commerce */
.product-box {
  padding: 0;
  border: 1px solid #e7eaec;
}
.product-box:hover,
.product-box.active {
  border: 1px solid transparent;
  -webkit-box-shadow: 0 3px 7px 0 #a8a8a8;
  -moz-box-shadow: 0 3px 7px 0 #a8a8a8;
  box-shadow: 0 3px 7px 0 #a8a8a8;
}
.product-imitation {
  text-align: center;
  padding: 90px 0;
  background-color: #f8f8f9;
  color: #bebec3;
  font-weight: 600;
}
.cart-product-imitation {
  text-align: center;
  padding-top: 30px;
  height: 80px;
  width: 80px;
  background-color: #f8f8f9;
}
.product-imitation.xl {
  padding: 120px 0;
}
.product-desc {
  padding: 20px;
  position: relative;
}
.ecommerce .tag-list {
  padding: 0;
}
.ecommerce .fa-star {
  color: #d1dade;
}
.ecommerce .fa-star.active {
  color: #f8ac59;
}
.ecommerce .note-editor {
  border: 1px solid #e7eaec;
}
table.shoping-cart-table {
  margin-bottom: 0;
}
table.shoping-cart-table tr td {
  border: none;
  text-align: right;
}
table.shoping-cart-table tr td.desc,
table.shoping-cart-table tr td:first-child {
  text-align: left;
}
table.shoping-cart-table tr td:last-child {
  width: 80px;
}
.product-name {
  font-size: 16px;
  font-weight: 600;
  color: #676a6c;
  display: block;
  margin: 2px 0 5px 0;
}
.product-name:hover,
.product-name:focus {
  color: #1ab394;
}
.product-price {
  font-size: 14px;
  font-weight: 600;
  color: #ffffff;
  background-color: #1ab394;
  padding: 6px 12px;
  position: absolute;
  top: -32px;
  right: 0;
}
.product-detail .ibox-content {
  padding: 30px 30px 50px 30px;
}
.image-imitation {
  background-color: #f8f8f9;
  text-align: center;
  padding: 200px 0;
}
.product-main-price small {
  font-size: 10px;
}
.product-images {
  margin: 0 20px;
}

.ibox {
  clear: both;
  margin-bottom: 25px;
  margin-top: 0;
  padding: 0;
}
.ibox.collapsed .ibox-content {
  display: none;
}
.ibox.collapsed .fa.fa-chevron-up:before {
  content: "\f078";
}
.ibox.collapsed .fa.fa-chevron-down:before {
  content: "\f077";
}
.ibox:after,
.ibox:before {
  display: table;
}
.ibox-title {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #ffffff;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 3px 0 0;
  color: inherit;
  margin-bottom: 0;
  padding: 14px 15px 7px;
  min-height: 48px;
}
.ibox-content {
  background-color: #ffffff;
  color: inherit;
  padding: 15px 20px 20px 20px;
  border-color: #e7eaec;
  border-image: none;
  border-style: solid solid none;
  border-width: 1px 0;
}
.ibox-footer {
  color: inherit;
  border-top: 1px solid #e7eaec;
  font-size: 90%;
  background: #ffffff;
  padding: 10px 15px;
}
         </style>
    </head>
    <!--Coded with love by Mutiullah Samim-->
    <body>
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Articulos disponibles</h2>
                <p class="text-center">Articulos desde la base de datos</p>
            </div>
           <div class="container">
            <%	
            ProductoDAO dao= new ProductoDAO();
			ArrayList<ProductoM> productosDB = dao.listarProductos();
			
			
			//Object s = request.getAttribute("listaProductos");
			System.out.println(productosDB.size());
			Iterator<ProductoM> it = productosDB.iterator();
			ArrayList<ProductoM> productosRow= new ArrayList<>();
			int cont=0; 
			while (it.hasNext()) {
				ProductoM producto =it.next();
				cont++;
				productosRow.add(producto);
				System.out.println(productosRow.size());
				
				if(cont>3 || it.hasNext()==false){
					%>
					<div class="row"><% 
					for(int i=0;i<cont;i++){
						System.out.println("Dentro del for");
						ProductoM aux=productosRow.get(i);
						String nombre,fabricante,proveedor,descripcion;
						int cantidad;
						double precio;

						nombre=aux.getNombre();
						fabricante=aux.getFabricante();
						proveedor = aux.getProveedor();
						descripcion = aux.getDescripcion();
						cantidad = aux.getCantidad();
						precio = aux.getPrecio();
						
						%>
						<div class="col-md-3">
        <div class="ibox">
            <div class="ibox-content product-box">
                <div class="product-imitation">
                    [ imagen no disponible ]
                </div>
                <div class="product-desc">
                    <span class="product-price">
                        $<%=precio %>
                    </span>
                    <small class="text-muted">Fabricante :<%=fabricante %></small>
                    <small class="text-muted">Disponibles :<%=cantidad %></small>
                    <small class="text-muted">Proveedor :<%=proveedor %></small>
                    <a href="#" class="product-name"> <%=nombre %></a>

                    <div class="small m-t-xs">
                        <%=descripcion %>
                    </div>
                    <div class="m-t text-righ">

                        <a href="#" class="btn btn-xs btn-outline btn-primary">Agregar <i class="fa fa-long-arrow-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	                	<%
					}
					cont=0;
					productosRow.clear();
					%></div><%
					}
				}
				%>
					</div>
					



             
            		
			
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </body>
</html>
<%@include file="/vistas/footer/footer.jsp"%>