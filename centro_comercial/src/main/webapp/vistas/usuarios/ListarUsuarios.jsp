<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.develop.model.UsuarioM"%>
<%@ page import="com.develop.DAO.UsuarioDAO"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>LISTA DE PRODUCTOS</h1>
		<table>
			<caption>Lista de Usuarios</caption>
			<thead>
				<tr>
					<th>Id usuario</th>
					<th>Nombre</th>
					<th>Usuario</th>
					<th>Password</th>
					<th>Email</th>
					<th>Direccion</th>
					<th>Fecha de registro</th>
				</tr>
			</thead>
			<%
			
			UsuarioDAO dao= new UsuarioDAO();
			ArrayList<UsuarioM> usuariosDB = dao.listarUsuarios();
			
			//Object s = request.getAttribute("listaProductos");
			
			Iterator<UsuarioM> it = usuariosDB.iterator();
			
			while (it.hasNext()) {
				UsuarioM u = it.next();
				System.out.println(u.toString());
				String id_usuario = String.valueOf(u.getIdUsuario());//Nombre
				String nombre = u.getNombre();//Nombre
				String usuario= u.getUsuario();//Usuario
				String password =u.getPass();//Password
				String email = u.getEmail();//Email
				String direccion = u.getDireccion();//Direccion
				String fechaR = u.getFechaRegistro().toString().replace("T", " ");// Fecha de registro
			%>
			<tr>
				<td>
					<%=id_usuario %>
				</td>
				<td>
					<%=nombre %>
				</td>
				<td>
					<%=usuario %>
				</td>
				<td>
					<%=password%>
				</td>
				<td>
				<%=email%>
				</td>
				<td>
				<%=direccion%>
				</td>
				<td>
				<%=fechaR%>
				</td>
			</tr>
			<%
			}
			%>
			<tbody>

			</tbody>
		</table>
</body>
</html>