<%@include file="/vistas/header/header.jsp"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.io.FileWriter"%>
<%@ page import="java.io.File"%>
<%@ page import="com.develop.model.UsuarioM"%>
<%@ page import="com.develop.DAO.UsuarioDAO"%>
<%@page import="com.itextpdf.text.*"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap CRUD Data Table for Database with Modal Form</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body {
	color: #566787;
	background: #f5f5f5;
	font-family: 'Varela Round', sans-serif;
	font-size: 13px;
}

.table-responsive {
	margin: 30px 0;
}

.table-wrapper {
	min-width: 1000px;
	background: #fff;
	padding: 20px 25px;
	border-radius: 3px;
	box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
}

.table-title2 {
	padding-bottom: 15px;
	background: #f5f5f5;
	color: #fff;
	padding: 16px 30px;
	margin: -20px -25px 10px;
	border-radius: 3px 3px 0 0;
}

.table-title2 h2 {
	margin: 5px 0 0;
	font-size: 24px;
}

.table-title2 .btn-group {
	float: right;
}

.table-title2 .btn {
	color: #fff;
	float: right;
	font-size: 13px;
	border: none;
	min-width: 50px;
	border-radius: 2px;
	border: none;
	outline: none !important;
	margin-left: 10px;
}

.table-title2 .btn i {
	float: left;
	font-size: 21px;
	margin-right: 5px;
}

.table-title2 .btn span {
	float: left;
	margin-top: 2px;
}

table.table tr th, table.table tr td {
	border-color: #e9e9e9;
	padding: 12px 15px;
	vertical-align: middle;
}

table.table tr th:first-child {
	width: 60px;
}

table.table tr th:last-child {
	width: 100px;
}

table.table-striped tbody tr:nth-of-type(odd) {
	background-color: #fcfcfc;
}

table.table-striped.table-hover tbody tr:hover {
	background: #f5f5f5;
}

table.table th i {
	font-size: 13px;
	margin: 0 5px;
	cursor: pointer;
}

table.table td:last-child i {
	opacity: 0.9;
	font-size: 22px;
	margin: 0 5px;
}

table.table td a {
	font-weight: bold;
	color: #566787;
	display: inline-block;
	text-decoration: none;
	outline: none !important;
}

table.table td a:hover {
	color: #2196F3;
}

table.table td a.edit {
	color: #FFC107;
}

table.table td a.delete {
	color: #F44336;
}

table.table td i {
	font-size: 19px;
}

table.table .avatar {
	border-radius: 50%;
	vertical-align: middle;
	margin-right: 10px;
}

.pagination {
	float: right;
	margin: 0 0 5px;
}

.pagination li a {
	border: none;
	font-size: 13px;
	min-width: 30px;
	min-height: 30px;
	color: #999;
	margin: 0 2px;
	line-height: 30px;
	border-radius: 2px !important;
	text-align: center;
	padding: 0 6px;
}

.pagination li a:hover {
	color: #666;
}

.pagination li.active a, .pagination li.active a.page-link {
	background: #03A9F4;
}

.pagination li.active a:hover {
	background: #0397d6;
}

.pagination li.disabled i {
	color: #ccc;
}

.pagination li i {
	font-size: 16px;
	padding-top: 6px
}

.hint-text {
	float: left;
	margin-top: 10px;
	font-size: 13px;
}
/* Custom checkbox */
.custom-checkbox {
	position: relative;
}

.custom-checkbox input[type="checkbox"] {
	opacity: 0;
	position: absolute;
	margin: 5px 0 0 3px;
	z-index: 9;
}

.custom-checkbox label:before {
	width: 18px;
	height: 18px;
}

.custom-checkbox label:before {
	content: '';
	margin-right: 10px;
	display: inline-block;
	vertical-align: text-top;
	background: white;
	border: 1px solid #bbb;
	border-radius: 2px;
	box-sizing: border-box;
	z-index: 2;
}

.custom-checkbox input[type="checkbox"]:checked+label:after {
	content: '';
	position: absolute;
	left: 6px;
	top: 3px;
	width: 6px;
	height: 11px;
	border: solid #000;
	border-width: 0 3px 3px 0;
	transform: inherit;
	z-index: 3;
	transform: rotateZ(45deg);
}

.custom-checkbox input[type="checkbox"]:checked+label:before {
	border-color: #03A9F4;
	background: #03A9F4;
}

.custom-checkbox input[type="checkbox"]:checked+label:after {
	border-color: #fff;
}

.custom-checkbox input[type="checkbox"]:disabled+label:before {
	color: #b8b8b8;
	cursor: auto;
	box-shadow: none;
	background: #ddd;
}
/* Modal styles */
.modal .modal-dialog {
	max-width: 400px;
}

.modal .modal-header, .modal .modal-body, .modal .modal-footer {
	padding: 20px 30px;
}

.modal .modal-content {
	border-radius: 3px;
}

.modal .modal-footer {
	background: #ecf0f1;
	border-radius: 0 0 3px 3px;
}

.modal .modal-title {
	display: inline-block;
}

.modal .form-control {
	border-radius: 2px;
	box-shadow: none;
	border-color: #dddddd;
}

.modal textarea.form-control {
	resize: vertical;
}

.modal .btn {
	border-radius: 2px;
	min-width: 100px;
}

.modal form label {
	font-weight: normal;
}
</style>
<script>
	$(document).ready(function() {
		// Activate tooltip
		$('[data-toggle="tooltip"]').tooltip();

		// Select/Deselect checkboxes
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function() {
			if (this.checked) {
				checkbox.each(function() {
					this.checked = true;
				});
			} else {
				checkbox.each(function() {
					this.checked = false;
				});
			}
		});
		checkbox.click(function() {
			if (!this.checked) {
				$("#selectAll").prop("checked", false);
			}
		});
	});
</script>
</head>
<body>
	<div class="container2">
		<div class="table-responsive">
			<div class="table-wrapper">
				<div class="table-title2">
					<div class="row">
						<div class="col-xs-6">
							<a href="#addEmployeeModal" class="btn btn-success"
								data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar nuevo usuario</span></a> 
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th><span class="custom-checkbox"> <input
									type="checkbox" id="selectAll"> <label for="selectAll"></label>
							</span></th>
							<th>Id_usuario</th>
							<th>Nombre</th>
							<th>Usuario</th>
							<th>Password</th>
							<th>Email</th>
							<th>Direcc�n</th>
							<th>Fecha de registro</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						<%
						UsuarioDAO dao= new UsuarioDAO();
						String url = "C:/Users/diego/Documents/miCarpetaFormatos/ListaUsuarios.pdf";
						String respuesta = dao.generarPdf(url);
						ArrayList<UsuarioM> usuariosDB = dao.listarUsuarios();
						int id_seleccion;
						System.out.println(usuariosDB.size());
						//Object s = request.getAttribute("listaProductos");
						
						Iterator<UsuarioM> it = usuariosDB.iterator();
						
						File carpeta = new File("C:/Users/diego/Documents/miCarpetaFormatos");
						File archivo = new File(carpeta.getPath()+"/ListaUsuarios.txt");
						
						try	{
							boolean banCarp = carpeta.mkdir();
							boolean bandera = archivo.createNewFile();
							FileWriter filewriter = new FileWriter(archivo);
								
						while (it.hasNext()) {
							UsuarioM u = it.next();
							System.out.println(u.toString());
							
							String id_usuario = String.valueOf(u.getIdUsuario());//id
							filewriter.write(id_usuario+", ");
							
							String nombre = u.getNombre();//Nombre
							filewriter.write(nombre+", ");
							
							String usuario= u.getUsuario();//Usuario
							filewriter.write(usuario+", ");
							
							String password =u.getPass();//Password
							filewriter.write(password+", ");
							
							String email = u.getEmail();//Email
							filewriter.write(email+", ");
							
							String direccion = u.getDireccion();//Direccion
							filewriter.write(direccion+", ");
							
							String fechaR = u.getFechaRegistro().toString().replace("T", " ");// Fecha de registro
							filewriter.write(fechaR);
							filewriter.write("\n ");
							%>
						<tr>
							<td><span class="custom-checkbox"> <input
									type="checkbox" id="checkbox1" name="options[]" value="1">
									<label for="checkbox1"></label>
							</span></td>
							<td><%=id_usuario %></td>
							<td><%=nombre %></td>
							<td><%=usuario%></td>
							<td><%=password%></td>
							<td><%=email%></td>
							<td><%=direccion%></td>
							<td><%=fechaR%></td>
							<td>
							<form method="post" action="<%=basePath%>UsuarioBorrarC?x=<%=id_usuario%>">
									<button  type="submit" name ="botonEdit" class="material-icons" id="myBtn" value="<%=id_usuario%>" >&#xE14C;</button>
							</form>
							<form method="post" action="<%=basePath%>UsuarioEditarC?n=0&a=<%=id_usuario%>&b=<%=nombre%>&c=<%=usuario%>&d=<%=password%>&e=<%=email%>&f=<%=direccion%>">
									 <button type="submit" name =" button" class="material-icons" >&#xE3C9;</button>
							</form>
							</td>
						</tr>
						<%
						}
							
							filewriter.close();
						}catch(Exception e){
							System.out.println("No se pudo crear el archivo");
						}
						%>
					</tbody>
				</table>
				<form method="post" action="<%=basePath %>vistas/productos/ListarProductos.jsp">
					<input class="btn btn-primary" type="submit" value="Productos">
				</form>
				<form method="post" action="<%=basePath %>vistas/productos/ListarProductos.jsp">
					<input class="btn btn btn-success" type="submit" value="Generar PDF">
				</form>
			</div>
		</div>
		
		
		
	</div>
	<!-- Edit Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form method="post" action="<%=basePath%>UsuarioNuevoC">
					<div class="modal-header">
						<h4 class="modal-title">Agregar Usuario</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control item" name="name"
								id="name" placeholder="Nombre">
						</div>
						<div class="form-group">
							<input type="text" class="form-control item" name="user"
								id="username" placeholder="Usuario">
						</div>
						<div class="form-group">
							<input type="password" class="form-control item" name="pass"
								id="password" placeholder="Contrase�a">
						</div>
						<div class="form-group">
							<input type="text" class="form-control item" name="email"
								id="email" placeholder="Correo electr�nico">
						</div>
						<div class="form-group">
							<input type="text" class="form-control item" name="dir"
								id="address" placeholder="Direccion">
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel">
						<button type="submit" class="btn btn-success" value="Registrar">Registrar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Edit Modal HTML -->
	<div id="editEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">
						<h4 class="modal-title">Edit Employee</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Id</label> <input type="text" name="bookId" id="bookId" value="">
						</div>
						<div class="form-group">
							<label>Name</label> <input type="text" class="form-control"
								required>
						</div>
						<div class="form-group">
							<label>Email</label> <input type="email" class="form-control"
								required>
						</div>
						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control" required></textarea>
						</div>
						<div class="form-group">
							<label>Phone</label> <input type="text" class="form-control"
								required>
						</div>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input type="submit" class="btn btn-info"
							value="Save">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form method ="post" action="<%=basePath%>UsuarioBorrarC">
					<div class="modal-header">
						<h4 class="modal-title">Eliminar Usuario</h4>
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<p>Estas seguro que deseas eliminar el usuario?<input type="text" ></p>
						<p class="text-warning">
							<small>Esta acci�n no podr� revertirse.</small>
						</p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal"
							value="Cancel"> <input type="submit"
							class="btn btn-danger" value="Delete">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<%@include file="../footer/footer.jsp"%>