<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="/vistas/header/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Easiest Way to Add Input Masks to Your Forms</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<%=basePath %>vistas/registrar/registrar.css">
</head>
<body>
	<%
		String msg = (String)session.getAttribute("a");
		String[] subs =msg.split("-");
		String id,nombre,usuario,password,email,direccion;
		id =subs[0];
		nombre=subs[1];
		usuario = subs[2];
		password = subs[3];
		email = subs[4];
		direccion = subs[5];
	
	%>
    <div class="registration-form">
        <form method="post" action="<%=basePath%>UsuarioEditarC">
            <div class="form-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" name="id" id="id" placeholder="<%=id%>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" name="nombre" id="nombre" placeholder="<%=nombre%>">
            </div>
            <div class="form-group">
                <input type="password" class="form-control item" name="usuario" id="usuario" placeholder="<%=usuario%>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" name="password" id="password" placeholder="<%=password%>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" name="email" id="email" placeholder="<%=email%>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control item" name="direccion" id="direccion" placeholder="<%=direccion%>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block create-account">Actualizar datos</button>
            </div>
        </form>
        
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
    