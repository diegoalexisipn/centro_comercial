
package com.develop.model;

import java.time.LocalDateTime;

public class ProductoM {
    private String nombre;
    private String fabricante;
    private String proveedor;
    private double precio;
    private int cantidad;
    private String descripcion;
    private LocalDateTime fechaRegistro;
	private int id;
	
    public ProductoM() {
		super();
	}

	public ProductoM(String nombre, String fabricante, String proveedor, String descripcion, int cantidad,double precio,LocalDateTime fechaRegistro) {
		super();
		this.nombre = nombre;
		this.fabricante = fabricante;
		this.proveedor = proveedor;
		this.precio = precio;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
		this.fechaRegistro = fechaRegistro;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ProductoM [nombre=" + nombre + ", fabricante=" + fabricante + ", proveedor=" + proveedor + ", precio="
				+ precio + ", cantidad=" + cantidad + ", descripcion=" + descripcion + ", fechaRegistro="
				+ fechaRegistro + "]";
	}



	
	
}
