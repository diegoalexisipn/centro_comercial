package com.develop.model;

import java.time.LocalDateTime;

public class UsuarioM {
	private int id_usuario;
	private String nombre;
	private String pass;
	private String usuario;
	private String direccion;
	private String email;
	private LocalDateTime fechaRegistro;
	
	
	
	public UsuarioM() {
		super();
	}
	public UsuarioM(String nombre,String usuario, String pass, String email,String direccion, LocalDateTime fechaRegistro) {
		super();
		this.nombre = nombre;
		this.pass = pass;
		this.usuario = usuario;
		this.direccion = direccion;
		this.email = email;
		this.fechaRegistro = fechaRegistro;
	}
	public int getIdUsuario() {
		return id_usuario;
	}
	public void setIdUsuario(int id) {
		id_usuario=id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LocalDateTime getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(LocalDateTime fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	@Override
	public String toString() {
		return "UsuarioM [nombre=" + nombre + ", pass=" + pass + ", usuario=" + usuario + ", direccion=" + direccion
				+ ", email=" + email + ", fechaRegistro=" + fechaRegistro + "]";
	}						
}
