package com.develop.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.develop.model.ProductoM;
import com.itextpdf.text.DocumentException;

public interface ProductoI {
	public String agregarProducto(ProductoM producto);
	public ArrayList<ProductoM> listarProductos();
	public String generarPdf(String carpeta) throws FileNotFoundException, DocumentException, IOException ;
}
