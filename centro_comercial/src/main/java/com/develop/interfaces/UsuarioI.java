package com.develop.interfaces;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.develop.model.UsuarioM;
import com.itextpdf.text.DocumentException;

public interface UsuarioI {
	public String validaUsuario(String nombre, String password);
	public String registrarUsuario(UsuarioM usuario);
	public ArrayList<UsuarioM> listarUsuarios();
	public String eliminarUsuario(int id);
	public String editarUsuario(UsuarioM usuario);
	public String generarPdf(String url) throws FileNotFoundException, DocumentException;
}
