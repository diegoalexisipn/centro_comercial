package com.develop.controller;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class UsuarioEditarC
 */
@WebServlet("/UsuarioEditarC")
public class UsuarioEditarC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioEditarC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String op= request.getParameter("n");
		if(op.equals("0")) {
			String id, nombre,usuario, password, email, direccion;
			id = request.getParameter("a");
			nombre = request.getParameter("b");
			usuario = request.getParameter("c");
			password =request.getParameter("d");
			email = request.getParameter("e");
			direccion =request.getParameter("f");
			
			String mensaje = id+"-"+nombre+"-"+usuario+"-"+password+"-"+email+"-"+direccion;
			System.out.println(mensaje);
			
			RequestDispatcher rq;
			rq =request.getRequestDispatcher("/vistas/admin/adminPrincipal.jsp");
			rq.forward(request, response);
		}
		else {
			String id, nombre,usuario, password, email, direccion;
			id = request.getParameter("id");
			nombre = request.getParameter("nombre");
			usuario = request.getParameter("usuario");
			password =request.getParameter("password");
			email = request.getParameter("email");
			direccion =request.getParameter("direccion");
			
			LocalDateTime fregistro = LocalDateTime.now();
			
			System.out.println(nombre+" "+usuario+" "+password+" "+email+" "+direccion);
			UsuarioDAO dao =new UsuarioDAO();
			UsuarioM newUser= new UsuarioM(nombre,usuario,password,email,direccion,fregistro);
			
			String respuesta = dao.editarUsuario(newUser);
			
			if(respuesta.equals("ok")) {
				System.out.println("Redireccionamiento");
				
				RequestDispatcher rq;	//redireccionador de jsp
				rq =request.getRequestDispatcher("/vistas/admin/adminPrincipal.jsp");
				rq.forward(request, response);
			}
			else {
				RequestDispatcher rq;
				rq =request.getRequestDispatcher("/vistas/Error.jsp");
				rq.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
