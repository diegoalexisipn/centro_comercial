package com.develop.controller;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.DAO.UsuarioDAO;
import com.develop.model.ProductoM;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class ProductoNuevoC
 */
@WebServlet("/ProductoNuevoC")
public class ProductoNuevoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductoNuevoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String nombre;
		String fabricante;
		String proveedor;
		String descripcion;
		int cantidad;
		double precio;
		
		nombre = request.getParameter("nombre");
		fabricante = request.getParameter("fabricante");//nombre de etiqueta input name=
		proveedor = request.getParameter("proveedor");
		descripcion = request.getParameter("descripcion");
		cantidad = Integer.parseInt(request.getParameter("cantidad"));
		precio = Double.parseDouble(request.getParameter("precio"));
		System.out.println(nombre+" "+fabricante+" "+proveedor+" "+descripcion+" "+cantidad+" "+precio);
		
		LocalDateTime fregistro = LocalDateTime.now();
		ProductoDAO dao =new ProductoDAO();
		
		ProductoM newProduct= new ProductoM(nombre,fabricante,proveedor,descripcion,cantidad,precio,fregistro);
		
		String respuesta = dao.agregarProducto(newProduct);
		
		if(respuesta.equals("ok")) {
			System.out.println("Redireccionamiento");
			RequestDispatcher rq;	//redireccionador de jsp
			rq =request.getRequestDispatcher("/vistas/ProductoAgregado.jsp");
			rq.forward(request, response);
		}
		else {
			RequestDispatcher rq;
			rq =request.getRequestDispatcher("/vistas/ProductoInvalido.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
