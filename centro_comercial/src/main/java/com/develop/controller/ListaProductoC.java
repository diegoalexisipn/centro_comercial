package com.develop.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductoDAO;
import com.develop.model.ProductoM;

/**
 * Servlet implementation class ListaProductoC
 */
@WebServlet("/ListaProductoC")
public class ListaProductoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListaProductoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		ProductoDAO dao= new ProductoDAO();

	        
	        
		ArrayList<ProductoM> productosDB = dao.listarProductos();
		ArrayList<String> producto = new ArrayList<>();
		ArrayList<ArrayList<String>> productosString= new ArrayList<>();
		for(ProductoM p: productosDB) {
			producto.add(p.getNombre());
			producto.add(p.getFabricante());
			producto.add(p.getProveedor());
			producto.add(p.getDescripcion());
			producto.add(String.valueOf(p.getCantidad()));
			producto.add(String.valueOf(p.getPrecio()));
			productosString.add(producto);
		}
		
		request.setAttribute("listaProductos", productosString);
		request.getRequestDispatcher("/vistas/productos/ListaProductos.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
