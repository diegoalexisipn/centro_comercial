package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

/**
 * Servlet implementation class UsuarioBorrar
 */

@WebServlet("/UsuarioBorrarC")
public class UsuarioBorrarC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioBorrarC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		int id;
		
		id = Integer.parseInt(request.getParameter("x"));
		UsuarioDAO dao =new UsuarioDAO();
		
		String respuesta = dao.eliminarUsuario(id);
		
		if(respuesta.equals("ok")) {
			System.out.println("Eliminado");
			
			RequestDispatcher rq;	//redireccionador de jsp
			rq =request.getRequestDispatcher("/vistas/admin/adminPrincipal.jsp");
			rq.forward(request, response);
		}
		else {
			RequestDispatcher rq;
			rq =request.getRequestDispatcher("/vistas/Error.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
