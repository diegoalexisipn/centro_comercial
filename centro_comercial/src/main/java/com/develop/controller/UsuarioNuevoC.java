package com.develop.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class UsuarioNuevoC
 */
@WebServlet("/UsuarioNuevoC")
public class UsuarioNuevoC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public UsuarioNuevoC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String usuario;
		String password;
		String nombre;
		String email;
		String direccion;
		
		nombre = request.getParameter("name");
		usuario = request.getParameter("user");//nombre de etiqueta input name=
		password = request.getParameter("pass");
		email = request.getParameter("email");
		direccion = request.getParameter("dir");
		
		LocalDateTime fregistro = LocalDateTime.now();
		
		
		System.out.println(nombre+" "+usuario+" "+password+" "+email+" "+direccion);
		UsuarioDAO dao =new UsuarioDAO();
		UsuarioM newUser= new UsuarioM(nombre,usuario,password,email,direccion,fregistro);
		
		String respuesta = dao.registrarUsuario(newUser);
		
		if(respuesta.equals("ok")) {
			System.out.println("Redireccionamiento");
			
			RequestDispatcher rq;	//redireccionador de jsp
			rq =request.getRequestDispatcher("/vistas/admin/adminPrincipal.jsp");
			rq.forward(request, response);
		}
		else {
			RequestDispatcher rq;
			rq =request.getRequestDispatcher("/vistas/Error.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	

}
