package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class UsuarioC
 */
@WebServlet("/UsuarioC")
public class UsuarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String usuario;
		String password;
		
		usuario = request.getParameter("usuario");//nombre de etiqueta input name=
		password = request.getParameter("password");
		System.out.println(usuario+" "+password);
		
		UsuarioDAO dao =new UsuarioDAO();
		String respuesta = dao.validaUsuario(usuario, password);
		System.out.println(respuesta);
		
		if(respuesta.equals("admin")) {
			RequestDispatcher rq;	//redireccionador de jsp
			rq =request.getRequestDispatcher("/vistas/admin/adminPrincipal.jsp");
			rq.forward(request, response);
		}else
		if(respuesta.equals("user")) {
			RequestDispatcher rq;	//redireccionador de jsp
			rq =request.getRequestDispatcher("/vistas/principal/principal.jsp");
			rq.forward(request, response);
		}
		else {
			RequestDispatcher rq;
			rq =request.getRequestDispatcher("/vistas/Error.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
