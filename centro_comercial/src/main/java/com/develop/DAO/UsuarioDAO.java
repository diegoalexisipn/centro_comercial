package com.develop.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.develop.config.ConexionDB;
import com.develop.interfaces.UsuarioI;
import com.develop.model.ProductoM;
import com.develop.model.UsuarioM;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;


public class UsuarioDAO implements UsuarioI{
	ConexionDB conections = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet  rs;

	@Override
	public String validaUsuario(String nombre,String password) {
		String sqladmin="SELECT * from encargadosdpto WHERE usuario='"+nombre+"'and password='"+password+"'";
		
		String sqluser="SELECT * from usuarios WHERE usuario='"+nombre+"'and password='"+password+"'";
		String user="";
		String pass="";
		try {
			consql = conections.getConection();
			ps= consql.prepareStatement(sqladmin);
			rs= ps.executeQuery();
			while(rs.next()){
                user = rs.getString("usuario");
                pass = rs.getString("password");
                if(nombre.equals(user) && password.equals(pass) && user!="") {
    				return "admin";
    			}
            }
			ps= consql.prepareStatement(sqluser);
			rs= ps.executeQuery();
			while(rs.next()){
                user = rs.getString("usuario");
                pass = rs.getString("password");
            }
			if(nombre.equals(user) && password.equals(pass)) {
            	return "user";
            }
		}
		catch(Exception e) {
			
		}
		return " ";
	}
	
	@Override
	public String registrarUsuario(UsuarioM usuario) {
		
		String sql = "insert into usuarios(nombre, usuario, password, email, direccion, fechaRegistro) values(?,?,?,?,?,?)";
		
		try {
			String s = validaUsuario(usuario.getUsuario(),usuario.getPass());
			if(s=="user" || s=="admin" ) {
				return "ok";				//FALTA VALIDAR EL REGRESO
			}else {
				consql = conections.getConection();
				ps= consql.prepareStatement(sql);
				ps.setString(1, usuario.getNombre());
				ps.setString(2, usuario.getUsuario());
				ps.setString(3, usuario.getPass());
				ps.setString(4, usuario.getEmail());
				ps.setString(5, usuario.getDireccion());
				ps.setTimestamp(6,Timestamp.valueOf(usuario.getFechaRegistro()));
				ps.executeUpdate();
	            return "ok";
				}
            }
		catch(Exception e) {
			System.out.println(e);
		}
		return "";
	}
	
	@Override
	public ArrayList<UsuarioM> listarUsuarios() {
		String nombre,usuario,password,email,direccion;
		LocalDateTime fregistro;
		ArrayList<UsuarioM> usuarios=new ArrayList<>();
		int id_usuario;
    try {
    	String sql = "SELECT * FROM usuarios";
        
        consql = conections.getConection();
        consql = conections.getConection();
		ps= consql.prepareStatement(sql);
		rs= ps.executeQuery();
		
		while(rs.next()){
			System.out.println("h");
			UsuarioM user;
			id_usuario= rs.getInt("id_usuario")+1000;
            nombre = rs.getString("nombre");
            usuario = rs.getString("usuario");
            password = rs.getString("password");
            email = rs.getString("email");
            direccion = rs.getString("direccion");
            fregistro =rs.getTimestamp("fechaRegistro").toLocalDateTime();
     
            user= new UsuarioM(nombre,usuario,password,email,direccion,fregistro);
            user.setIdUsuario(id_usuario);
            usuarios.add(user); 
        }
		System.out.println(usuarios.size());
        	return usuarios;
    } catch (SQLException ex) {
    	
    }
	
		return usuarios;
	}

	@Override
	public String eliminarUsuario(int id) {
		String sql = "DELETE FROM usuarios WHERE id_usuario="+id;
		System.out.println("consulta: "+sql);
		try {
			consql = conections.getConection();
			ps= consql.prepareStatement(sql);
			ps.executeUpdate();
            return "ok";
            }
		catch(SQLException e) {
			System.out.println("No se encontr� en la BD");
			return "No se pudo eliminar el elemento en la BD";
		}
	}
	@Override
	public String editarUsuario(UsuarioM usuario) {
		String sql = "UPDATE usuarios SET nombre ="+usuario.getNombre()+", usuario ="+usuario.getIdUsuario()+", password="+usuario.getPass()+", email="+usuario.getEmail()+",direccion="+usuario.getDireccion()+" WHERE id_usuario="+usuario.getIdUsuario();
		try {
			consql = conections.getConection();
			ps= consql.prepareStatement(sql);
			ps.executeUpdate();
            return "ok";
            }
		catch(SQLException e) {
			System.out.println("No se encontr� en la BD");
			return "";
		}
	}

	@Override
	public String generarPdf(String url) throws FileNotFoundException, DocumentException {
		String nombre,usuario,password,email,direccion;
		LocalDateTime fregistro;
		int id_usuario;
		
		Document doc = new Document();


	    try {
	    	String sql = "SELECT * FROM usuarios";
	        
	        consql = conections.getConection();
	        consql = conections.getConection();
			ps= consql.prepareStatement(sql);
			rs= ps.executeQuery();
			
			FileOutputStream ficheroPdf = new FileOutputStream(url); //Se crea y se almacena el PDF en la ruta especificada.
			PdfWriter.getInstance(doc, ficheroPdf); //Se solicita escribir en el documento.
			doc.open(); // Se abre el documento para poder escribir en �l .
			
			while(rs.next()){
				
				id_usuario= rs.getInt("id_usuario");
	            nombre = rs.getString("nombre");
	            usuario = rs.getString("usuario");
	            password = rs.getString("password");
	            email = rs.getString("email");
	            direccion = rs.getString("direccion");
	            fregistro =rs.getTimestamp("fechaRegistro").toLocalDateTime();
	            
	            Paragraph p = new Paragraph(" "+id_usuario+" ,"+nombre+" ,"+usuario+" ,"+password+" ,"+email+" ,"+ direccion+" ,"+fregistro);
	            doc.add(p);
	        }
			doc.close();
	    } catch (SQLException ex) {
	    	
	    }
			return "PDF generado";
	}
	
}
