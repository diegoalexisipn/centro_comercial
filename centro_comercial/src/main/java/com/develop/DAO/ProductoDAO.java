package com.develop.DAO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.develop.config.ConexionDB;
import com.develop.interfaces.ProductoI;
import com.develop.model.ProductoM;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class ProductoDAO implements ProductoI {
	ConexionDB conections = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet  rs;
	ProductoM producto = new ProductoM();
	ArrayList<ProductoM> productos=new ArrayList<>();
	
	@Override
	public String agregarProducto(ProductoM producto) {
		
		String sql = "insert into productos(nombre, fabricante, proveedor, descripcion, cantidad, precio, fechaRegistro) values(?,?,?,?,?,?,?)";
		
		try {
			consql = conections.getConection();
			ps= consql.prepareStatement(sql);
			ps.setString(1,producto.getNombre());
			ps.setString(2,producto.getFabricante());
			ps.setString(3,producto.getProveedor());
			ps.setString(4,producto.getDescripcion());
			ps.setInt(5,producto.getCantidad());
			ps.setDouble(6,producto.getPrecio());
			ps.setTimestamp(7,Timestamp.valueOf(producto.getFechaRegistro()));
			ps.executeUpdate();
            return "ok";
            }
		catch(Exception e) {
			System.out.println(e);
		}
		return "";
	}
	
	@Override
	public ArrayList<ProductoM> listarProductos() {
		String nombre,fabricante,proveedor,descripcion;
		int cantidad,id;
		double precio;
		LocalDateTime fregistro;
		  
    try {
    	String sql = "SELECT * FROM productos";
        
        consql = conections.getConection();
        consql = conections.getConection();
		ps= consql.prepareStatement(sql);
		rs= ps.executeQuery();
		
		while(rs.next()){
			ProductoM producto;
			id= Integer.parseInt(rs.getString("id_producto"));
            nombre = rs.getString("nombre");
            fabricante = rs.getString("fabricante");
            proveedor = rs.getString("proveedor");
            descripcion = rs.getString("descripcion");
            cantidad = Integer.parseInt(rs.getString("cantidad"));
            precio = Double.parseDouble(rs.getString("precio"));
            fregistro =rs.getTimestamp("fechaRegistro").toLocalDateTime();
            
            producto= new ProductoM(nombre,fabricante,proveedor,descripcion,cantidad,precio, fregistro);
            producto.setId(id);
            productos.add(producto);
        }
        	return productos;
    } catch (SQLException ex) {
    	
    }
		return productos;
	}
	
	@Override
	public String generarPdf(String carpeta) throws DocumentException, IOException {
		String nombre,fabricante,proveedor,descripcion;
		int cantidad,id;
		double precio;
		LocalDateTime fregistro;
		
		Document doc = new Document();
		String url1 =carpeta+"ListaProductos.txt";
		File archivo = new File(url1);
		
    try {
    	String sql = "SELECT * FROM productos";
        
        consql = conections.getConection();
        consql = conections.getConection();
		ps= consql.prepareStatement(sql);
		rs= ps.executeQuery();
		
		FileOutputStream ficheroPdf = new FileOutputStream(carpeta+"ListaProductos.pdf"); //Se crea y se almacena el PDF en la ruta especificada.
		PdfWriter.getInstance(doc, ficheroPdf); //Se solicita escribir en el documento.
		doc.open(); // Se abre el documento para poder escribir en �l .
		
		boolean bandera = archivo.createNewFile();
		FileWriter filewriter = new FileWriter(archivo);
		
		while(rs.next()){
			
			id= Integer.parseInt(rs.getString("id_producto"));
            nombre = rs.getString("nombre");
            fabricante = rs.getString("fabricante");
            proveedor = rs.getString("proveedor");
            descripcion = rs.getString("descripcion");
            cantidad = Integer.parseInt(rs.getString("cantidad"));
            precio = Double.parseDouble(rs.getString("precio"));
            fregistro =rs.getTimestamp("fechaRegistro").toLocalDateTime();
            
            Paragraph p = new Paragraph(" "+id+" ,"+nombre+" ,"+fabricante+" ,"+proveedor+" ,"+descripcion+" ,"+ cantidad+" ,"+precio+" ,"+fregistro);
            doc.add(p);
            filewriter.write(id+" ,");
            filewriter.write(nombre+" ,");
            filewriter.write(fabricante+" ,");
            filewriter.write(proveedor+" ,");
            filewriter.write(descripcion+" ,");
            filewriter.write(cantidad+" ,");
            filewriter.write(precio+" ,");
            filewriter.write(fregistro+" ,");
            filewriter.write("\n");
        }
			filewriter.close();
        	doc.close();
    } catch (SQLException ex) {
    	
    }
		return "Archivo no generado";
	}
}
